import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Scanner;

/* Cracking passwords.
 * 
 * Author: Geanina Tambaliuc
 * 
 */
public class PasswCracking {
	
	public static void main(String[] args) throws FileNotFoundException {
		
	 	//ask for the filename
       System.out.println("Please enter the file name: ");
        Scanner s1=new Scanner(System.in);
        String fileName=s1.nextLine();
        File passwordfile=new File(fileName);
       
        //create an ArrayList to store the users info
        ArrayList<User> users = new ArrayList<User>(); 
        int count=0; //used to count how many fails
        long startTime = System.nanoTime();
        try
        {	Scanner scanFile=new Scanner(passwordfile);
        	String row="";
        	
        	//read the file
        	while(scanFile.hasNext())
        	{
        		row=scanFile.nextLine();
        		//Splitting each row by :
        		String[] splitting=row.split(":");
        		User user=new User();
        		user.setUsername(splitting[0]);
        		user.setPassword(splitting[1]);
        		
        		String[] name= splitting[4].split(" ");
        		user.setFname(name[0]);
        		user.setLname(name[1]);
        		
        		users.add(user);
        	}
        }catch(Exception e)
        {
        	System.out.println(e.getMessage());
        }
        
        try
        {for(User user: users)
        {
        	String salt=user.getPassword().substring(0, 2);
        	
        	/*First check the most common passwords */
        	
        	//check if the password is the username 
        	if(jcrypt.crypt(salt,user.getUsername()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getUsername());
        	}
        	
        	//check if the password is the LowerCase Username 
        	else if(jcrypt.crypt(salt,user.getUsername().toLowerCase()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getUsername().toLowerCase());
        	}
        	
        	//check if the password is the Uppercase Username 
        	else if(jcrypt.crypt(salt,user.getUsername().toUpperCase()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getUsername().toUpperCase());
        	}
        	
        	//check if the password is "password"
        	else if(jcrypt.crypt(salt,"password").equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+"password");
        	}
        	
        	//check if the password is first name
        	else if(jcrypt.crypt(salt,user.getFname()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getFname());
        	}
        	
        	//check if the password is last name
        	else if(jcrypt.crypt(salt,user.getLname()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getLname());
        	}
        	
        	//check if the password is last & first name e.g.SmithJohn
        	else if(jcrypt.crypt(salt,user.getLname()+user.getFname()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getLname()+user.getFname());
        	}
        	
        	//check if the password is first & last name
        	else if(jcrypt.crypt(salt,user.getFname()+user.getLname()).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getFname()+user.getLname());
        	}
        	
        	//check if the password is first & last name e.g JohnS
        	else if(jcrypt.crypt(salt,user.getFname()+user.getLname().substring(0, 1)).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getFname()+user.getLname().substring(0, 1));
        	}
        	
        	//check if the password is last & first name e.g.SmithJ
        	else if(jcrypt.crypt(salt,user.getLname()+user.getFname().substring(0, 1)).equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+user.getLname()+user.getFname().substring(0, 1));
        	}
        	
        	//check if the password is 123456
        	else if(jcrypt.crypt(salt,"123456").equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+"123456");
        	}
        	
        	//check if the password is 12345678
        	else if(jcrypt.crypt(salt,"12345678").equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+"12345678");
        	}
        	
        	//check if the password is qwerty
        	else if(jcrypt.crypt(salt,"qwerty").equals(user.getPassword()))
        	{
        		System.out.println("Username: "+user.getUsername()+"  Password: "+"qwerty");
        	}
        	
        	/*Check the mangles*/
        	else
        	{
        		//check mangles with username
        		int check=OneMangle(user,user.getUsername(),salt);
        		if(check==1)
        		{
        			//password was found
        		}
        		else 
        		{
        			//check with 2 mangles
        			check=twoMangles(user,user.getUsername(),salt);
        			if(check==1)
        			{
        				//password was found
        			}
        			else
        			{
        				//check mangles with firstName
        				check=OneMangle(user,user.getFname(),salt);
        				if(check==1)
        				{
        					//password was found
        				}
        				else
        				{
        					//check with 2 mangles
        					check=twoMangles(user,user.getFname(),salt);
        					if(check==1)
        					{
        						//password was found
        					}
        					else
        					{
        						//check mangles with lastName
        						check=OneMangle(user,user.getLname(),salt);
        						if(check==1)
        						{
        							//password was found
        						}
        						else
        						{
        							//check with 2 mangles
        							check=twoMangles(user,user.getLname(),salt);
        							if(check==1)
        							{
        								//password was found
        							}
        							else
        							{
        								//check one mangle with words from the dictionary
        								
        								File filename=new File("dictionary.txt");
        								Scanner readDictionary= new Scanner(filename);
        						        int ok=0, ok2=0; 
        								//reading the dictionary 
        								while(readDictionary.hasNext())
        								{
        									check=OneMangle(user,readDictionary.nextLine(),salt);
        									if(check==1)
        									{
        										//password found 
        										ok=1;
        										break;
        									}
        								}
        								readDictionary.close();
        								
        								if(ok==0)
        								{
        									//check two mangles with words from the dictionary
        									File filename2=new File("dictionary.txt");
            								Scanner readDictionary2= new Scanner(filename2);
    
            								//reading the dictionary 
            								while(readDictionary2.hasNext())
            								{
            									check=twoMangles(user,readDictionary2.nextLine(),salt);
            									if(check==1)
            									{
            										//password found 
            										ok2=1;
            										break;
            									}
            								}
            								readDictionary2.close();
            								if(ok2==0)
            								{
            									System.out.println("Couldn't crack password for the username: " +user.getUsername());
            									count++;
            								}
        								}
        								
        								
        							}
        						}
        								
        					}
        				}
        			}
        		}
        	}
        	
        }
        }catch(Exception e)
        {
        	System.out.println(e.getMessage());
        }

        long endTime = System.nanoTime();
		long timeElapsed = endTime - startTime;


		System.out.println("Total Execution Time (milliseconds) : " + 
								timeElapsed / 1000000);
		
		int cracked=20-count;
		System.out.println(cracked + " out of 20 passwords cracked");
	}
	
	/**
	 * Function to return all the possible permutations of 2 mangles
	 * set containing all the possible mangle functions
		 * a- prepend
		 * b- append
		 * c- deleteFirst
		 * d- deleteLast
		 * e- reverse
		 * f- duplicate
		 * g- reflectFirst
		 * h- reflectLast
		 * i- uppercase
		 * j- lowercase
		 * k- capitalize
		 * l- ncapitalize
		 * m- toggleUp
		 * n- toggleLow
	 * @return per - all the possible mangles permutations
	 */
	private static HashSet<String> findMangles()
	{
		
		char[] set= {'a','b','c','d','e','f','g','h','i','j','k','l','m','n'};
		
		//the length of the set
		int setLength=set.length;
		
		HashSet<String> pers=new HashSet<String>();
		//finding the mangles recursively
		return findManglesRecursively(set,setLength,"",2,pers);
		
	}
	/**
	 * Finding the mangles recursively
	 * @param set - all the possible mangles
	 * @param setLength - the length of the set
	 * @param string - the permutation
	 * @param s - the size of the permutation
	 * @param per - the set containing the possible permutations
	 * @return per - all the possible mangle permutations
	 */
	private static HashSet<String> findManglesRecursively(char[] set, int setLength, String string, int s, HashSet<String> per) {
		
		//base case - finding a key and adding it to the set
		if (s==0)
		{
			per.add(string);
			return per;
		}
		
		//combine all the characters from the set one by one
		for(int j=0;j<setLength;++j)
		{
			//adding the next character from the set
			String newPer=string+set[j];
			
			//recursive call (decreased the s because I added a new character)
			findManglesRecursively(set,setLength,newPer,s-1,per);
		}
		return per;
	}

	/**check all mangles
	 *if password found ->return 1
	 *otherwise -> return 0 */
	private static int OneMangle(User user, String string, String salt) {
		//prepend mangle
		String[] prependResult = prepend(string);
		for(int i=0;i<prependResult.length;i++)
		{
			if(jcrypt.crypt(salt,prependResult[i]).equals(user.getPassword()))
			{
    		System.out.println("Username: "+user.getUsername()+"  Password: "+prependResult[i]);
    		return 1;
			}
		}
		
		//append mangle
		String[] appendResult = append(string);
		for(int i=0;i<appendResult.length;i++)
		{
			if(jcrypt.crypt(salt,appendResult[i]).equals(user.getPassword()))
			{
    		System.out.println("Username: "+user.getUsername()+"  Password: "+appendResult[i]);
    		return 1;
			}
		}
		
		//deleteFirst
		if(jcrypt.crypt(salt,deleteFirst(string)).equals(user.getPassword()))
		{
		System.out.println("Username: "+user.getUsername()+"  Password: "+deleteFirst(string));
		return 1;
		}
		
		//deleteLast
		if(jcrypt.crypt(salt,deleteLast(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+deleteLast(string));
			return 1;
		}
				
		//reverse
		if(jcrypt.crypt(salt,reverse(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+reverse(string));
			return 1;
		}
		
		//duplicate
		if(jcrypt.crypt(salt,duplicate(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+duplicate(string));
			return 1;
		}
		
		//reflectFirst
		if(jcrypt.crypt(salt,reflectFirst(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+reflectFirst(string));
			return 1;
		}
		
		//reflectLast
		if(jcrypt.crypt(salt,reflectLast(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+reflectLast(string));
			return 1;
		}
				
		//Uppercase
		if(jcrypt.crypt(salt,uppercase(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+uppercase(string));
			return 1;
		}
		
		//Lowercase
		if(jcrypt.crypt(salt,lowercase(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+lowercase(string));
			return 1;
		}
		
		//capitalize
		if(jcrypt.crypt(salt,capitalize(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+capitalize(string));
			return 1;
		}
		
		//ncapitalize
		if(jcrypt.crypt(salt,ncapitalize(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+ncapitalize(string));
			return 1;
		}
		
		//toggleUp
		if(jcrypt.crypt(salt,toggleUp(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+toggleUp(string));
			return 1;
		}
		
		//toggleLow
		if(jcrypt.crypt(salt,toggleLow(string)).equals(user.getPassword()))
		{
			System.out.println("Username: "+user.getUsername()+"  Password: "+toggleLow(string));
			return 1;
		}
		
	  return 0;
	}
	
	/**check all mangles (permutations of 2)
	 *if password found ->return 1
	 *otherwise -> return 0 */
	public static int twoMangles(User user, String string, String salt)
	{
		HashSet<String> permutations = findMangles();
		
		for(String per: permutations)
		{
			String mangle1=per.substring(0,1);
			String mangle2=per.substring(1);
			String stringToMangle=string;
			
			
			//First mangle
			if(mangle1.equals("c"))
			{
				stringToMangle=deleteFirst(stringToMangle);
			}
			
			else if(mangle1.equals("d"))
			{
				stringToMangle=deleteLast(stringToMangle);
			}
			else if(mangle1.equals("e"))
			{
				stringToMangle=reverse(stringToMangle);
			}
			
			else if(mangle1.equals("f"))
			{
				stringToMangle=duplicate(stringToMangle);
			}
			else  if(mangle1.equals("g"))
			{
				stringToMangle=reflectFirst(stringToMangle);
			}
			else if(mangle1.equals("h"))
			{
				stringToMangle=reflectLast(stringToMangle);
			}
			else if(mangle1.equals("i"))
			{
				stringToMangle=uppercase(stringToMangle);
			}
			else if(mangle1.equals("j"))
			{
				stringToMangle=lowercase(stringToMangle);
			}
			else if(mangle1.equals("k"))
			{
				stringToMangle=capitalize(stringToMangle);
			}
			else if(mangle1.equals("l"))
			{
				stringToMangle=ncapitalize(stringToMangle);
			}
			else if(mangle1.equals("m"))
			{
				stringToMangle=toggleUp(stringToMangle);
			}
			else if(mangle1.equals("n"))
			{
				stringToMangle=toggleLow(stringToMangle);
			}
			
			//Second mangle
			if(mangle2.equals("c"))
			{
				stringToMangle=deleteFirst(stringToMangle);
			}
			else if(mangle2.equals("d"))
			{
				stringToMangle=deleteLast(stringToMangle);
			}
			else if(mangle2.equals("e"))
			{
				stringToMangle=reverse(stringToMangle);
			}
			
			else if(mangle2.equals("f"))
			{
				stringToMangle=duplicate(stringToMangle);
			}
			else  if(mangle2.equals("g"))
			{
				stringToMangle=reflectFirst(stringToMangle);
			}
			else if(mangle2.equals("h"))
			{
				stringToMangle=reflectLast(stringToMangle);
			}
			else if(mangle2.equals("i"))
			{
				stringToMangle=uppercase(stringToMangle);
			}
			else if(mangle2.equals("j"))
			{
				stringToMangle=lowercase(stringToMangle);
			}
			else if(mangle2.equals("k"))
			{
				stringToMangle=capitalize(stringToMangle);
			}
			else if(mangle2.equals("l"))
			{
				stringToMangle=ncapitalize(stringToMangle);
			}
			else if(mangle2.equals("m"))
			{
				stringToMangle=toggleUp(stringToMangle);
			}
			else if(mangle2.equals("n"))
			{
				stringToMangle=toggleLow(stringToMangle);
			}
			
			//check if the password was found
			if(jcrypt.crypt(salt,stringToMangle).equals(user.getPassword()))
			{
				System.out.println("Username: "+user.getUsername()+"  Password: "+stringToMangle);
				return 1;
			}
			
			//Prepend  mangles
			if(mangle1.equals("a"))
			{
				String[] resultsPre=prepend(stringToMangle);
				
				for(int i=0;i<resultsPre.length;i++)
				{
					if(mangle2.equals("c"))
					{
						stringToMangle=deleteFirst(resultsPre[i]);
					}
					else if(mangle2.equals("d"))
					{
						stringToMangle=deleteLast(resultsPre[i]);
					}
					else if(mangle2.equals("e"))
					{
						stringToMangle=reverse(resultsPre[i]);
					}
					
					else if(mangle2.equals("f"))
					{
						stringToMangle=duplicate(resultsPre[i]);
					}
					else  if(mangle2.equals("g"))
					{
						stringToMangle=reflectFirst(resultsPre[i]);
					}
					else if(mangle2.equals("h"))
					{
						stringToMangle=reflectLast(resultsPre[i]);
					}
					else if(mangle2.equals("i"))
					{
						stringToMangle=uppercase(resultsPre[i]);
					}
					else if(mangle2.equals("j"))
					{
						stringToMangle=lowercase(resultsPre[i]);
					}
					else if(mangle2.equals("k"))
					{
						stringToMangle=capitalize(resultsPre[i]);
					}
					else if(mangle2.equals("l"))
					{
						stringToMangle=ncapitalize(resultsPre[i]);
					}
					else if(mangle2.equals("m"))
					{
						stringToMangle=toggleUp(resultsPre[i]);
					}
					else if(mangle2.equals("n"))
					{
						stringToMangle=toggleLow(resultsPre[i]);
					}
					
					//check if the password was found
					if(jcrypt.crypt(salt,stringToMangle).equals(user.getPassword()))
					{
						System.out.println("Username: "+user.getUsername()+"  Password: "+stringToMangle);
						return 1;
					}
				}
				
				//if we have a prepend and an append
				for(int i=0;i<resultsPre.length;i++)
				{
					if(mangle2.equals("b"))
					{
						String[] resultsAp= append(resultsPre[i]);
						for(int j=0;j<resultsAp.length;j++)
						{
							//check if the password was found
							if(jcrypt.crypt(salt,resultsAp[j]).equals(user.getPassword()))
							{
								System.out.println("Username: "+user.getUsername()+"  Password: "+resultsAp[j]);
								return 1;
							}
						}
					}
				}
			}
				//Append mangles
				if(mangle1.equals("b"))
				{
					String[] resultsAp=append(stringToMangle);
					
					for(int i=0;i<resultsAp.length;i++)
					{
						if(mangle2.equals("c"))
						{
							stringToMangle=deleteFirst(resultsAp[i]);
						}
						else if(mangle2.equals("d"))
						{
							stringToMangle=deleteLast(resultsAp[i]);
						}
						else if(mangle2.equals("e"))
						{
							stringToMangle=reverse(resultsAp[i]);
						}
						
						else if(mangle2.equals("f"))
						{
							stringToMangle=duplicate(resultsAp[i]);
						}
						else  if(mangle2.equals("g"))
						{
							stringToMangle=reflectFirst(resultsAp[i]);
						}
						else if(mangle2.equals("h"))
						{
							stringToMangle=reflectLast(resultsAp[i]);
						}
						else if(mangle2.equals("i"))
						{
							stringToMangle=uppercase(resultsAp[i]);
						}
						else if(mangle2.equals("j"))
						{
							stringToMangle=lowercase(resultsAp[i]);
						}
						else if(mangle2.equals("k"))
						{
							stringToMangle=capitalize(resultsAp[i]);
						}
						else if(mangle2.equals("l"))
						{
							stringToMangle=ncapitalize(resultsAp[i]);
						}
						else if(mangle2.equals("m"))
						{
							stringToMangle=toggleUp(resultsAp[i]);
						}
						else if(mangle2.equals("n"))
						{
							stringToMangle=toggleLow(resultsAp[i]);
						}
						
						//check if the password was found
						if(jcrypt.crypt(salt,stringToMangle).equals(user.getPassword()))
						{
							System.out.println("Username: "+user.getUsername()+"  Password: "+stringToMangle);
							return 1;
						}
					}
					
					//if we have a prepend and an append
					for(int i=0;i<resultsAp.length;i++)
					{
						if(mangle2.equals("a"))
						{
							String[] resultsPre= prepend(resultsAp[i]);
							for(int j=0;j<resultsPre.length;j++)
							{
								//check if the password was found
								if(jcrypt.crypt(salt,resultsPre[j]).equals(user.getPassword()))
								{
									System.out.println("Username: "+user.getUsername()+"  Password: "+resultsPre[j]);
									return 1;
								}
							}
						}
					}
				
						
			}
				
				//Other mangles with prepend
				if(mangle2.equals("a"))
				{
					String[] resultsPre= prepend(stringToMangle);
					for(int j=0;j<resultsPre.length;j++)
					{
						//check if the password was found
						if(jcrypt.crypt(salt,resultsPre[j]).equals(user.getPassword()))
						{
							System.out.println("Username: "+user.getUsername()+"  Password: "+resultsPre[j]);
							return 1;
						}
					}
				}
				
				//other mangles with append
				if(mangle2.equals("b"))
				{
					String[] resultsAp= append(stringToMangle);
					for(int j=0;j<resultsAp.length;j++)
					{
						//check if the password was found
						if(jcrypt.crypt(salt,resultsAp[j]).equals(user.getPassword()))
						{
							System.out.println("Username: "+user.getUsername()+"  Password: "+resultsAp[j]);
							return 1;
						}
					}
				}
		}
		
		
		
		return 0;
	}
	
	/*mangle for prepend a character to a string 
	 * */	
	private static String[] prepend(String string) {
			
		//set containing all the possible characters from the keyboard
				char[] set= {'`','1','2','3','4','5','6','7','8','9','0','-','=','\\','!','~','@','#','$','%','^','&','*','(',')','_','+','|',
						'q','w','e','r','t','y','u','i','o','p','[',']','{','}','a','s','d','f','g','h','j','k','l',';','"','\'','z','x','c',
						'v','b','n','m',',','.','/','<','>','?','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L',
						'Z','X','C','V','B','N','M',' '};
		String[] toReturn = new String[94]; 
		int i=0;
		for(char ch : set)
		{
			toReturn[i]=ch+string;
			i++;
				 	
		}
						
		return toReturn;
	}
	
	/*mangle for append a character to a string 
	 **/	
	private static String[] append(String string) {
		
		//set containing all the possible characters from the keyboard
				char[] set= {'`','1','2','3','4','5','6','7','8','9','0','-','=','\\','!','~','@','#','$','%','^','&','*','(',')','_','+','|',
						'q','w','e','r','t','y','u','i','o','p','[',']','{','}','a','s','d','f','g','h','j','k','l',';','"','\'','z','x','c',
						'v','b','n','m',',','.','/','<','>','?','Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L',
						'Z','X','C','V','B','N','M',' '};
				
		String[] toReturn = new String[94]; 
		int i=0;
		for(char ch : set)
		{
			toReturn[i]=string+ch;
			i++;
			 	
		}
				
		return toReturn;
	}
	
	/*delete the first character from the string
	 **/
	private static String deleteFirst( String string)
	{
		//delete first char
		string=string.substring(1);
		
		return string;
	}
	
	/*delete the last character from the string
	 **/
	private static String deleteLast(String string)
	{
		//delete last char
		string=string.substring(0,string.length()-1);
		return string;
	}
	
	
	/*reverse the string
	 **/
	private static String reverse(String string)
	{
		//reverse the string
		 String reverse="";
		 for(int i = string.length() - 1; i >= 0; i--)
	        {
	            reverse = reverse + string.charAt(i);
	        }
		
		
		return reverse;
	}
	
	/*duplicate the string
	 **/
	private static String duplicate(String string)
	{
		//duplicate string
		string=string+string;
		
		return string;
	}
	
	/*reflect the string , e.g., gnirtsstring 
	 **/
	private static String reflectFirst(String string)
	{
		//reverse the string
		 String reverse="";
		 for(int i = string.length() - 1; i >= 0; i--)
	        {
	            reverse = reverse + string.charAt(i);
	        }
		
		 //reflect
		 string=reverse+string;
		 
		return string;
	}
	
	/*reflect the string , e.g., stringgnirts 
	 **/
	private static String reflectLast(String string)
	{
		//reverse the string
		 String reverse="";
		 for(int i = string.length() - 1; i >= 0; i--)
	        {
	            reverse = reverse + string.charAt(i);
	        }
		
		 //reflect
		string=string+reverse;
		
		return string;
	}
	
	/*uppercase mangle
	 **/
	private static String uppercase(String string)
	{
		
		string=string.toUpperCase();
		
		return string;
	}
	
	/*lowercase mangle
	 **/
	private static String lowercase(String string)
	{
		
		string=string.toLowerCase();
		
		return string;
	}
	
	/*capitalize mangle
	 **/
	private static String capitalize(String string)
	{
		//capitalize
		String cap = string.substring(0, 1).toUpperCase() + string.substring(1);
		
		
		return cap;
	}
	
	
	/*ncapitalize mangle
	 **/
	private static String ncapitalize(String string)
	{
		//ncapitalize
		String cap = string.substring(0, 1).toLowerCase() + string.substring(1).toUpperCase();
		
		
		return cap;
	}
	
	/*toggle mangle, eg. StRiNg
	 **/
	private static String toggleUp(String string)
	{
		String toggle="";
		for(int i=0;i<string.length();i=i+2)
		{
			String ch= String.valueOf(string.charAt(i));
			toggle=toggle+ch.toUpperCase();
			
			if((i+1)<string.length())
			{String ch2= String.valueOf(string.charAt(i+1));
			 toggle=toggle+ch2.toLowerCase();
			}
		}
		
		return toggle;
	}
	
	/*toggle mangle, eg.  sTrInG
	 **/
	private static String toggleLow(String string)
	{
		String toggle="";
		for(int i=0;i<string.length();i=i+2)
		{
			String ch= String.valueOf(string.charAt(i));
			toggle=toggle+ch.toLowerCase();
			
			if((i+1)<string.length())
			{String ch2= String.valueOf(string.charAt(i+1));
			 toggle=toggle+ch2.toUpperCase();
			}
		}
		
		return toggle;
	}
	
	/*User class used to store the user info*/
	public static class User
	{
		
		String username,password,fname,lname;
		
		public User(String username,String password, String fname, String lname)
		{
			this.username=username;
			this.password=password;
			this.fname=fname;
			this.lname=lname;
		}
		
		 public User() {
			
		}

		@Override
	        public String toString() {
	            return username+","+password+","+fname+","+lname+";";
	        }
		 
		 public void setUsername(String username) {
				this.username = username;
			}

			public void setPassword(String password) {
				this.password = password;
			}

			public void setFname(String fname) {
				this.fname = fname;
			}

			public void setLname(String lname) {
				this.lname = lname;
			}

			public String getUsername() {
				return username;
			}

			public String getPassword() {
				return password;
			}

			public String getFname() {
				return fname;
			}

			public String getLname() {
				return lname;
			}
			

	}
	
}
